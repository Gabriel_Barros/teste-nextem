const menu = {
  bebidas: [{cerveja: 4.2}, {suco: 2.5}],
  lanche: [{hamburguer: 6.9}, {pizza: 20.3}]
};

function somaValores(data) {

  const total = []; 
  const menus = Object.values(data);
  
  if(!menus.length) return 0;

  const reducer = (accumulator, currentValue) => accumulator + currentValue;

  menus.map((single) => {
      single.map((produto) => {
        const preco = Object.entries(produto)[0][1]
        if(preco) total.push(preco);
      })
  })

  return total.reduce(reducer);
}

console.log(somaValores(menu));