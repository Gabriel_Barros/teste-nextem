let timeToResponse = null;

const sendRequestToApi = () => new Promise((resolve) => {
  const time = timeToResponse || parseInt(500 * Math.random());
  setTimeout(() => resolve({ time, fromApi: true }), time);
});

function sendRequestWithTimeout() {
  return new Promise((resolve, reject) => {
    setTimeout(function() {
      reject({timeout: true})
    }, 500)

    sendRequestToApi().then(resolve, reject)
  })
}

sendRequestWithTimeout().then(data => {
  console.log(data)
}).catch(err => {
  console.log(err)
})