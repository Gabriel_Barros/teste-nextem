-- O diretório autenticacao-server se trata da API responsável pela autenticação do usuário
-- O diretório reactjs/nextem-web se trata da aplicação web que consome a API 

// Instalação do NPM: 
 npm install

--Configurações para a API

//Instalação do AdonisJS: 
 npm i -g @adonisjs/cli

//Iniciar servidor:
 adonis serve --dev

//Instalação do Postgres:
 npm install pg

//Executar as migrações:
 adonis migration:run


--Configurações da Aplicação em ReactJs

//Dependências utilizadas:
 npm install react-router-dom axios styled-components prop-types font-awesome

//Subir a aplicação React:
 npm start


