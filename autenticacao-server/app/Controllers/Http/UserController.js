'use strict'

const User = use("App/Models/User")

class UserController {
    async create ({ request }) {                                     // No param request vão todos os dados da requisição
        const data = request.only(["username", "email", "password"]) // Obejto que recebe os parâmetros da requisição
        const user = await User.create(data)                         // Cria novo usário com os parâmetros do obejto data
        // await evita que a requisição retorne sucesso antes que o processo finalize 
        return user
    }
}

module.exports = UserController
