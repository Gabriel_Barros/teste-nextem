'use strict'

// Controller responsável pela autenticação
class SessionController {
    async create ({ request, auth }) {                    // Método que além de fazer a requisição, também faz uma autenticação
        const { email, password } = request.all()         // Traz o email e senha
        const token = await auth.attempt(email, password) // Faz o login com o email e senha autenticados
    
        return token
      }
}

module.exports = SessionController
