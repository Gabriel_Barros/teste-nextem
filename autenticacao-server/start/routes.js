'use strict'

const Route = use('Route')

Route.post('/users', 'UserController.create') // Rota que faz post no /users utilizando o método create do UserController
Route.post('/sessions', 'SessionController.create') // Rota que faz a autenticação e retorna o token