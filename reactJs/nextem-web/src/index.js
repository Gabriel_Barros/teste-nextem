/* eslint-disable no-unused-vars */

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Link, withRouter } from "react-router-dom";  // withRouter permite a mudança entre as páginas

import api from "./services/api";

ReactDOM.render(<App />, document.getElementById('root'));




