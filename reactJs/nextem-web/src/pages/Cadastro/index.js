/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import Logo from "./../../assests/icone-logo.png";
import api from "./../../services/api";

import { Form, Container } from "./styles";

class SignUp extends Component {
  state = {
    username: "",
    email: "",
    password: "",
    error: ""
  };

  handleSignUp = async e => {
    e.preventDefault();
    const { username, email, password } = this.state;
    if (!username || !email || !password) {
      this.setState({ error: "Preencha todos os dados para se cadastrar" });  // Verificação que retorna erro ao identificar que algum campo faltou ser preenchido
    } else {
      try {
        await api.post("/users", { username, email, password });  // Envia um Post com os dados inseridos
        this.props.history.push("/");                             // Redireciona para a rota de login
      } catch (err) {
        console.log(err);                                         // Retorna erro
        this.setState({ error: "Ocorreu um erro ao registrar." });
      }
    }
  };

  render() {
    return (
      <Container>
        <Form onSubmit={this.handleSignUp}>
          <img src={Logo} alt="Icone logo" />
          {this.state.error && <p>{this.state.error}</p>}
          <input
            type="text"
            placeholder="Nome de usuário"
            onChange={e => this.setState({ username: e.target.value })}
          />
          <input
            type="email"
            placeholder="Endereço de e-mail"
            onChange={e => this.setState({ email: e.target.value })}
          />
          <input
            type="password"
            placeholder="Senha"
            onChange={e => this.setState({ password: e.target.value })}
          />
          <button type="submit">Cadastre-se</button>
          <hr />
          <Link to="/">Fazer login</Link>
        </Form>
      </Container>
    );
  }
}

export default withRouter(SignUp);
