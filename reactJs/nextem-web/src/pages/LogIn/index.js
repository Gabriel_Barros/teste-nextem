/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import Logo from "./../../assests/icone-logo.png";
import api from "./../../services/api";
import { login } from "./../../services/auth";
import { Form, Container } from "./styles";

class SignIn extends Component {
    state = {
      email: "",
      password: "",
      error: ""
    };
  
    handleSignIn = async e => {  //Método que faz um try catch, para se faltar algum campo a ser preenchido, retorna um erro
      e.preventDefault();
      const { email, password } = this.state;
      if (!email || !password) {
        this.setState({ error: "Digite email e senha para continuar." });
      } else {
        try {
          const response = await api.post("/sessions", { email, password });
          login(response.data.token);  // Caso os campos estejam corretos, traz um token e guarda 
          this.props.history.push("/app");
        } catch (err) {
          this.setState({
            error:
              "Falha no login, verifique se seus dados estão corretos ou se cadastre."
          });
        }
      }
    };

    render() {
        return (
          <Container>
            <Form onSubmit={this.handleSignIn}>
              <img src={Logo} alt="Icone logo" />
              {this.state.error && <p>{this.state.error}</p>}
              <input
                type="email"
                placeholder="Endereço de e-mail"
                onChange={e => this.setState({ email: e.target.value })}
              />
              <input
                type="password"
                placeholder="Senha"
                onChange={e => this.setState({ password: e.target.value })}
              />
              <button type="submit">Entrar</button>
              <hr />
              <Link to="/cadastro">Cadastre-se</Link>
            </Form>
          </Container>
        );
      }
    }
    
    export default withRouter(SignIn);
