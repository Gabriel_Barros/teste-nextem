import axios from "axios";
import { getToken } from "./auth";

// Define a Api que vai ser consumida
const api = axios.create({
  baseURL: "http://127.0.0.1:3333"
});


api.interceptors.request.use(async config => {        // Intercepta uma requisição antes de ela acontecer
  const token = getToken();
if (token) {                                         
    config.headers.Authorization = `Bearer ${token}`; // Verifica se existe um token e add o header de authorization na requisição
  }
  return config;
});

export default api;